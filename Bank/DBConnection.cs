﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Bank.Model;
using System.Data;
using Bank.Services;

namespace Bank
{
    class DBConnection
    {
        TransactionEvent transactionEvent = new TransactionEvent();
        //creating sql connection object
        SqlConnection connect = new SqlConnection("Data Source=(LocalDB)\\" +
            "MSSQLLocalDB;AttachDbFilename" +
            "=C:\\WAMP64\\WWW\\BEZAO\\TASKS\\BANK\\BANK\\DATABASE1.MDF;" +
            "Integrated Security=True");
        public void Upload(Customers customers)
        {

            //create command object with sql query and link to object connection
            SqlCommand cmd = new SqlCommand("INSERT INTO Customers (Firstname, Lastname, AccountNumber, Age, Balance, Date)" +
                "VALUES(@customerFirstname, @customerLastname, @customerAccountNumber, @customerAge, @customerBalance, @date)",
                connect);
            //create the parameters method 1 (working fine)
            //set values to parameter from the console

            /*cmd.Parameters.AddWithValue("@customerFirstname", customers.Customer_firstname);
            cmd.Parameters.AddWithValue("@customerLastname", customers.Customer_lastname);
            cmd.Parameters.AddWithValue("@customerAccountNumber", customers.Account_number);
            cmd.Parameters.AddWithValue("@customerAge", customers.Age);
            cmd.Parameters.AddWithValue("@customerBalance", customers.Balance);
            cmd.Parameters.AddWithValue("@date", customers.Date);*/

            //create the parameters method 2 (working fine)
            //set values to parameter from the console
            cmd.Parameters.Add("@customerFirstname", SqlDbType.VarChar).Value = customers.Customer_firstname;
            cmd.Parameters.Add("@customerLastname", SqlDbType.VarChar).Value = customers.Customer_lastname;
            cmd.Parameters.Add("@customerAccountNumber", SqlDbType.NVarChar, 10).Value = customers.Account_number;
            cmd.Parameters.Add("@customerAge", SqlDbType.Int).Value = customers.Age;
            cmd.Parameters.Add("@customerBalance", SqlDbType.Decimal).Value = customers.Balance;
            cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = customers.Date;

            //open sql connection
            if (connect.State == ConnectionState.Closed)
                connect.Open();

            //execute the query and return number of rows affected, should be one
            try
            {
                int rowsAffected = cmd.ExecuteNonQuery();
                Console.WriteLine($"\n\t\tAccount Created on {customers.Date} is successful and your account number is {customers.Account_number}");
            }
            catch (Exception ex) { Console.WriteLine(ex); }

            //close connection when done
            connect.Close();
        }
        public void Withdraw(String account_number, decimal amount)
        {
            if (connect.State == ConnectionState.Open)
                connect.Close();

            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Customers WHERE AccountNumber = " + account_number, connect);


            //open connection
            connect.Open();
            //execute query
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            //read the outputed data from the database
            if (!sqlDataReader.Read())
                Console.WriteLine("\n\t\t\tThis account has not been registered with this bank");
            else
            {
                //get the current user account balance
                decimal balance = (decimal)sqlDataReader.GetValue(4);

                if (amount < 0)
                    Console.WriteLine("\n\t\t\tYou cannot withdraw a negative amount");
                if (balance - amount > 0)
                {
                    balance -= amount;
                    connect.Close();
                    //update the balance after the withdrawal
                    SqlCommand sqlCommand1 = new SqlCommand("UPDATE Customers SET Balance = " + balance + "WHERE AccountNumber = " + account_number, connect);
                    connect.Open();
                    sqlCommand1.ExecuteNonQuery();
                    //insert transaction and pass event
                    InsertTransactions(Transactions(account_number, amount, balance, DateTime.Now));
                    transactionEvent.TransactionAlert(Transactions(account_number, amount, balance, DateTime.Now));
                    //dispose and close the command
                    sqlCommand1.Dispose();
                    connect.Close();
                    Console.WriteLine("\n\t\t\tTransaction Successful");
                }
                else
                {
                    Console.WriteLine("\n\t\t\tInsufficient amount");
                }

            }

        }
        public void Deposit(String account_number, decimal amount)
        {
            if (connect.State == ConnectionState.Open)
                connect.Close();

            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Customers WHERE AccountNumber = " + account_number, connect);
            //read the outputed data from the database

            //open connection
            connect.Open();

            //execute query
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            //read the outputed data from the database
            if (sqlDataReader.Read())
            {
                //get the current user account balance
                decimal balance = (decimal)sqlDataReader.GetValue(4);

                if (amount < 0)
                    Console.WriteLine("\n\t\t\tYou cannot deposit a negative amount");

                //add something here
                else
                {
                    balance += amount;
                    connect.Close();
                    //update the balance after the withdrawal
                    SqlCommand sqlCommand1 = new SqlCommand("UPDATE Customers SET Balance = " + balance + "WHERE AccountNumber = " + account_number, connect);
                    connect.Open();
                    sqlCommand1.ExecuteNonQuery();
                    //insert transaction and pass event
                    InsertTransactions(Transactions(account_number, amount, balance, DateTime.Now));
                    transactionEvent.TransactionAlert(Transactions(account_number, amount, balance, DateTime.Now));
                    //dispose and close the command
                    sqlCommand1.Dispose();
                    connect.Close();
                    Console.WriteLine("\n\t\t\tTransaction Successful");
                }

            }
            else
            {
                Console.WriteLine("\n\t\t\tThis account has not been registered with this bank");
            }

        }
        public void CheckBalance(String account_number)
        {
            if (connect.State == ConnectionState.Open)
                connect.Close();

            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Customers WHERE AccountNumber = " + account_number, connect);


            //open connection
            connect.Open();
            //execute query
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            //read the outputed data from the database
            if (sqlDataReader.Read())
            {
                //get the current user account balance
                decimal balance = (decimal)sqlDataReader.GetValue(4);
                connect.Close();
                Console.WriteLine($"\n\t\t\tYour account balance is: " + balance);
            }
            else
            {
                Console.WriteLine("\n\t\t\tThis account has not been registered with this bank");
            }
        }
        public void AllCustomers()
        {
            if (connect.State == ConnectionState.Open)
                connect.Close();
            List<Customers> customers = new List<Customers>();
            int count = 1;
            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Customers", connect);


            //open connection
            connect.Open();
            //execute query
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            //read the outputed data from the database
            while (sqlDataReader.Read())
            {
                var obj = new Customers();
                obj.Customer_firstname = sqlDataReader.GetValue(0).ToString();
                obj.Customer_lastname = sqlDataReader.GetValue(1).ToString();
                obj.Account_number = sqlDataReader.GetValue(2).ToString();
                obj.Age = (int)sqlDataReader.GetValue(3);
                obj.Balance = (decimal)sqlDataReader.GetValue(4);
                obj.Date = (DateTime)sqlDataReader.GetValue(5);

                customers.Add(obj);
            }
            Console.WriteLine("\n\t\t\tSN\tFIRSTNAME\tLASTNAME\tACCOUNT NUMBER\tAGE\tBALANCE\tDATE");
            foreach (var item in customers)
            {
                Console.WriteLine($"\n\t\t\t{count}\t{item.Customer_firstname}\t{item.Customer_lastname}\t{item.Account_number}\t{item.Age}\t{item.Balance}\t{item.Date}");
                count++;
            }
        }
        public void AllTransactions()
        {
            if (connect.State == ConnectionState.Open)
                connect.Close();
            List<Transactions> transactions = new List<Transactions>();
            int count = 1;
            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Transactions", connect);


            //open connection
            connect.Open();
            //execute query
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            //read the outputed data from the database
            while (sqlDataReader.Read())
            {
                var obj = new Transactions();
                obj.Account_number = sqlDataReader.GetValue(0).ToString();
                obj.Amount = (decimal)sqlDataReader.GetValue(1);
                obj.Balance = (decimal)sqlDataReader.GetValue(2);
                obj.Transaction_date = (DateTime)sqlDataReader.GetValue(3);

                transactions.Add(obj);
            }
            Console.WriteLine("\n\t\t\tSN\tAccountNumber\tBalance\tDate");
            foreach (var item in transactions)
            {
                Console.WriteLine($"\n\t\t\t{count}\t{item.Account_number}\t{item.Amount}\t{item.Balance}\t{item.Transaction_date}");
                count++;
            }
        }
        public void AccountTransactions(string accountNumber)
        {
            if (connect.State == ConnectionState.Open)
                connect.Close();
            List<Transactions> transactions = new List<Transactions>();
            int count = 1;
            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Transactions WHERE AccountNumber = " + accountNumber, connect);


            //open connection
            connect.Open();
            //execute query
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            //read the outputed data from the database
            while (sqlDataReader.Read())
            {
                var obj = new Transactions();
                obj.Account_number = sqlDataReader.GetValue(0).ToString();
                obj.Amount = (decimal)sqlDataReader.GetValue(1);
                obj.Balance = (decimal)sqlDataReader.GetValue(2);
                obj.Transaction_date = (DateTime)sqlDataReader.GetValue(3);

                transactions.Add(obj);
            }
            Console.WriteLine("\n\t\t\tSN\tAccountNumber\tBalance\tDate");
            foreach (var item in transactions)
            {
                Console.WriteLine($"\n\t\t\t{count}\t{item.Account_number}\t{item.Amount}\t{item.Balance}\t{item.Transaction_date}");
                count++;
            }
        }
        public void FundTransfer(String accountNumber1, string accountNumber2, decimal amount)
        {
            if (connect.State == ConnectionState.Open)
                connect.Close();

            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Customers WHERE AccountNumber = " + accountNumber1, connect);
            
            //open connection
            connect.Open();

            //execute query
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            
            //read the outputed data from the database
            if (sqlDataReader.Read())
            {
                //get the sender account balance
                decimal senderBalance = (decimal)sqlDataReader.GetValue(4);
                connect.Close();
                if(senderBalance <= 0)
                    Console.WriteLine("Insufficent Fund");
                else
                {
                    SqlCommand sqlCommand2 = new SqlCommand("SELECT * FROM Customers WHERE AccountNumber = " + accountNumber2, connect);
                    // open connection
                    connect.Open();
                    SqlDataReader sqlDataReader1 = sqlCommand2.ExecuteReader();
                    if (sqlDataReader1.Read())
                    {
                        decimal receiverBalance = (decimal)sqlDataReader1.GetValue(4);

                        receiverBalance += amount;
                        senderBalance -= amount;
                        connect.Close();

                        SqlCommand sqlCommand3 = new SqlCommand("UPDATE Customers SET Balance = " + senderBalance + "WHERE AccountNumber = " + accountNumber1, connect);
                        connect.Open();
                        sqlCommand3.ExecuteNonQuery();
                        sqlCommand3.Dispose();
                        connect.Close();

                        SqlCommand sqlCommand4 = new SqlCommand("UPDATE Customers SET Balance = " + receiverBalance + "WHERE AccountNumber = " + accountNumber2, connect);
                        connect.Open();
                        sqlCommand4.ExecuteNonQuery();
                        sqlCommand4.Dispose();
                        connect.Close();

                        Console.WriteLine("n\t\t\tTransaction Successful");
                    }
                }
            }
        }
        public void InsertTransactions(Transactions transactions)
        {
            //create command object with sql query and link to object connection
            SqlCommand cmd = new SqlCommand("INSERT INTO Transactions (AccountNumber, Amount, Balance, Date)" +
                "VALUES(@customerAccountNumber, @amount,  @customerBalance, @date)",
                connect);

            // create the parameters method 2(working fine)
            //set values to parameter from the console
            cmd.Parameters.Add("@customerAccountNumber", SqlDbType.NVarChar, 10).Value = transactions.Account_number;
            cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = transactions.Amount;
            cmd.Parameters.Add("@customerBalance", SqlDbType.Decimal).Value = transactions.Balance;
            cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = transactions.Transaction_date;

            //open sql connection
            if (connect.State == ConnectionState.Closed)
                connect.Open();

            //execute the query and return number of rows affected, should be one
            try
            {
                int rowsAffected = cmd.ExecuteNonQuery();
                Console.WriteLine($"\n\t\t\tTransaction successfully saved");
            }
            catch (Exception ex) { Console.WriteLine(ex); }
        }
        public Transactions Transactions(string account_number, decimal amount, decimal balance, DateTime date)
        {
            Transactions transactions = new Transactions();
            transactions.Account_number = account_number;
            transactions.Amount = amount;
            transactions.Balance = balance;
            transactions.Transaction_date = DateTime.Now;

            return transactions;
        }
    }
}
