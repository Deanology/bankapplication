﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    class Customers
    {
        public string Customer_firstname { get; set; }
        public string Customer_lastname { get; set; }
        public string Account_number { get; set; }
        public int Age { get; set; }
        public DateTime Date { get; set; }
        public decimal Balance { get; set; }
    }
}
