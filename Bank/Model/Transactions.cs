﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    class Transactions
    {
        public string Account_number { get; set; }
        public DateTime Transaction_date { get; set; }
        public decimal Balance { get; set; }
        public decimal Amount { get; set; }
    }
}
