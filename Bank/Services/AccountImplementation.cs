﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Model;

namespace Bank.Services
{
    class AccountImplementation : Iinterface
    {
        DBConnection connection = new DBConnection();
        public void Choice()
        {
            Console.WriteLine("\n\t\t\t=====================================================");
            Console.WriteLine("\t\t\t\t WELCOME TO DEANOLOGY ATM SERVICES");
            Console.WriteLine("\t\t\t=====================================================\n\n");
            Console.WriteLine("\t\t\t1. Cash Withdrawal\t\t2. Bills Payment \n");
            Console.WriteLine("\t\t\t3. Fund Transfer\t\t4. Exit \n");
            Console.WriteLine("\t\t\t5. Account Opening\t\t6. Cash Deposit \n");
            Console.WriteLine("\t\t\t7. Balance\t\t\t8. All Customers \n");
            Console.WriteLine("\t\t\t9. All Transactions\t\t\t10. Single Transaction \n");
            Console.WriteLine("\n\t\t\t=====================================================");
        }
        public void Option()
        {
            Choice();
            int choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Withdrawal();
                    break;
                case 2:
                    Console.WriteLine("ready");
                    break;
                case 3:
                    FundTransfer();
                    break;
                case 4:
                    Exit();
                    break;
                case 5:
                    CreateAccount();
                    break;
                case 6:
                    Deposit();
                    break;
                case 7:
                    CheckBalance();
                    break;
                case 8:
                    AllCustomers();
                    break;
                case 9:
                    AllTransactions();
                    break;
                case 10:
                    AccountTransactions();
                    break;
                default:
                    Option();
                    break;
            }
        }
        public void CreateAccount()
        {
            Console.WriteLine("\n\t\t\t==========Fill in your bio data to create an account==========\n");
            Customers customers = new Customers();

            Console.WriteLine("\n\t\t\tEnter your firstname");
            customers.Customer_firstname = Console.ReadLine();

            Console.WriteLine("\n\t\t\tEnter your lastname");
            customers.Customer_lastname = Console.ReadLine();

            Console.WriteLine("\n\t\t\tEnter your age");
            customers.Age = int.Parse(Console.ReadLine());

            try
            {
                Console.WriteLine("\n\t\t\tEnter your initial Deposit");
                decimal deposit = decimal.Parse(Console.ReadLine());
                if (deposit < 0)
                {
                    Console.WriteLine("\n\t\t\tInitial Deposit cannot be negative");
                    customers.Balance = 0;
                }
                else
                {
                    customers.Balance = deposit;
                }
            }
            catch (Exception e) { Console.WriteLine(e); }

            customers.Date = DateTime.Now;
            customers.Account_number = accountNumber();

            //add customer to the db
            connection.Upload(customers);

            Option();
        }
        //Method to generate account Number
        public static string accountNumber()
        {
            Random rand = new Random();
            return rand.Next(0, 1000000).ToString("D10");
        }
        public void Withdrawal()
        {
            Console.WriteLine("Enter your account number");
            string account_number = Console.ReadLine();

            Console.WriteLine("\n\t\t\tEnter the amount you want to withdraw");
            decimal withdraw = decimal.Parse(Console.ReadLine());

            connection.Withdraw(account_number, withdraw);
            Option();
        }
        public void Deposit()
        {
            Console.WriteLine("Enter your account number");
            string account_number = Console.ReadLine();

            Console.WriteLine("\n\t\t\tEnter the amount you want to deposit");
            decimal withdraw = decimal.Parse(Console.ReadLine());

            connection.Deposit(account_number, withdraw);
            Option();
        }
        public void CheckBalance()
        {
            Console.WriteLine("Enter your account number");
            string account_number = Console.ReadLine();

            connection.CheckBalance(account_number);
            Option();
        }
        public void AllCustomers()
        {
            Console.WriteLine("\n\t\t\tList of all customers");
            connection.AllCustomers();
            Option();
        }
        public void AllTransactions()
        {
            Console.WriteLine("\n\t\t\tList of all transactions");
            connection.AllTransactions();
            Option();
        }
        public void AccountTransactions()
        {
            Console.WriteLine("Enter your account Number");
            string accountNumber = Console.ReadLine();

            connection.AccountTransactions(accountNumber);
        }
        public void FundTransfer()
        {
            Console.WriteLine("Enter your account number");
            string accountNumber1 = Console.ReadLine();

            Console.WriteLine("Enter receiver's account number");
            string accountNumber2 = Console.ReadLine();

            Console.WriteLine("Enter the amount you want to transfer");
            decimal amount = decimal.Parse(Console.ReadLine());

            connection.FundTransfer(accountNumber1, accountNumber2, amount);
            Option();
        }
        public void Exit()
        {
            System.Environment.Exit(0);
        }
    }
}
