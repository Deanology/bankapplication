﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bank.Model;
using System.Threading.Tasks;

namespace Bank.Services
{
    class TransactionEvent
    {
        public event EventHandler<TransactionEventsArgs> Trans;

        public void TransactionAlert(Transactions transactions)
        {
            TransactionMessage transactionmessage = new TransactionMessage();
            Trans = transactionmessage.Message;            
            Trans?.Invoke(this, new TransactionEventsArgs(transactions));
        }
    }
}
