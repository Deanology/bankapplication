﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Model;

namespace Bank.Services
{
    class TransactionEventsArgs
    {
        public Transactions transactions;
        public TransactionEventsArgs(Transactions transactions) => this.transactions = transactions;
    }
}
