﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Services
{
    class TransactionMessage
    {
        public void Message(object sender, TransactionEventsArgs e) =>
            Console.WriteLine($"\n\t\t\tAccount Number: {e.transactions.Account_number}\n\t\t\t" +
                $"Amount: {e.transactions.Amount}\n\t\t\tBalance: {e.transactions.Balance}\n\t\t\t" +
                $"Date: {e.transactions.Transaction_date}");
    }
}
